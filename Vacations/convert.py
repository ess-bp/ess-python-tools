from icalendar import Calendar, Event
import pytz
from datetime import datetime
import os

"""
@author Yngve Levinsen

This script use the modules

 - datetime
 - icalendar
 - pytz
"""

for_confluence = False
timezone = pytz.timezone("Europe/Stockholm")


def create_event(summary, reduced, date, uid):
    event = Event()
    event["uid"] = str(uid)
    if reduced.lower() == "office closed":
        dstart = datetime.strptime(date, "%Y-%m-%d").date()
    elif reduced.lower() == "reduced 3 hours":
        if for_confluence:
            t1 = 12
        else:
            t1 = 14
        t2 = t1 + 3

        dstart = datetime.strptime(f"{date} {t1}", "%Y-%m-%d %H")
        # note could here use datetime.replace(tzinfo=timezone), but seems Outlook works better without it?
        dend = datetime.strptime(f"{date} {t2}", "%Y-%m-%d %H")

        event.add("dtend", dend)
    else:
        print(f"WARNING, do not understand '{reduced}'")
        dstart = datetime.datetime.strptime(date, "%Y-%m-%d").date()
    event.add("dtstart", dstart)
    event["dtstamp"] = datetime.now().strftime("%Y%m%dT%H%M%SZ")
    event["description"] = ", ".join([summary, reduced])
    event["summary"] = summary
    event["X-MICROSOFT-CDO-BUSYSTATUS"] = "OOF"
    return event


# tz=Timezone()
# tz.add('tzid', 'Europe/Stockholm')

calendar_swe_all = Calendar()
calendar_swe_all.add("version", "2.0")
calendar_swe_all.add("prodid", "Yngve's Superscript")
calendar_eng_all = calendar_swe_all.copy()

# calendar_swe_all.add_component(tz)
# calendar_eng_all.add_component(tz)

i_swe_all = 10
i_eng_all = 10

for f in os.listdir("."):
    if f.split("_")[0] == "list":
        year = int(f.split(".")[0].split("_")[1])
        print(f, year)

        calendar_swe = Calendar()
        calendar_swe.add("version", "2.0")
        calendar_swe.add("prodid", "Yngve's Superscript")
        calendar_swe.add("X-WR-TIMEZONE", timezone)
        calendar_eng = calendar_swe.copy()

        # calendar_swe.add_component(tz)
        # calendar_eng.add_component(tz)

        i_swe = 10
        i_eng = 10

        has_weekday = False

        for line in open(f, "r", encoding="utf8"):
            if line.strip()[0] == "#":
                header = line.strip()[1:].split()
                i_desc = header.index("Holiday")
                i_date = header.index("Date")
                i_whours = header.index("Working_hours")
                if "Weekday" in header:
                    has_weekday = True
                continue
            # encoding issues with swedish:
            lsp = list(filter(None, line.split("  ")))
            date = lsp[i_date].strip()

            desc_eng = lsp[i_desc].split("/")[0].strip()
            desc_swe = lsp[i_desc].split("/")[1].strip()
            reduction = lsp[i_whours].strip()

            event_swe = create_event(desc_swe, reduction, date, i_swe)
            i_swe += 1
            calendar_swe.add_component(event_swe)

            event_eng = create_event(desc_eng, reduction, date, i_eng)
            i_eng += 1
            calendar_eng.add_component(event_eng)

            if year > 2016:
                event_swe_all = create_event(desc_swe, reduction, date, i_swe_all)
                i_swe_all += 1
                calendar_swe_all.add_component(event_swe_all)

                event_eng_all = create_event(desc_eng, reduction, date, i_eng_all)
                i_eng_all += 1
                calendar_eng_all.add_component(event_eng_all)

        open("vacations_swe_{}.ics".format(year), "wb").write(calendar_swe.to_ical())
        open("vacations_eng_{}.ics".format(year), "wb").write(calendar_eng.to_ical())
open("vacations_swe.ics", "wb").write(calendar_swe_all.to_ical())
open("vacations_eng.ics", "wb").write(calendar_eng_all.to_ical())
