# Date Holiday Working_hours
2015-01-01  New Year´s Day / Nyårsdagen  Office closed
2015-01-02  Briding Day / Klämdag  Office closed
2015-01-05  Briding Day / Klämdag  Office closed
2015-01-06  Ephiphany / Trettondedag jul   Office closed
2015-04-02  Maundy Thursday / Skärtorsdagen   Reduced 3 hours
2015-04-03  Good Friday/Långfredagen  Office closed
2015-04-06  Easter Monday / Annandag påsk  Office closed
2015-04-30  Eve of May Day / Valborgsmässoafton  Reduced 3 hours
2015-05-01  May Day / Första maj  Office closed
2015-05-14  Ascention Day / Kristi himmelfärds dag  Office closed
2015-05-15  Briding Day / Klämdag  Office closed
2015-06-06  Sweden´s National Day / Sveriges Nationaldag  Office closed
2015-06-19  Midsummer Eve / Midsommarafton  Office closed
2015-10-30  Eve of all Saints Day / Allhelgonaafton  Reduced 3 hours
2015-12-24  Christmas Eve / Julafton  Office closed
2015-12-25  Christmas Day / Juldagen  Office closed
2015-12-26  Boxing Day / Annandag jul  Office closed
2015-12-31  New Year´s Eve / Nyårsafton  Office closed
