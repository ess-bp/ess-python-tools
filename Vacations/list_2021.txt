# Weekday  Date  Holiday  Working_hours
Friday  2021-01-01  New Year’s Day / Nyårsdagen  Office closed
Tuesday  2021-01-05  Eve of Ephiphany / Trettondedag afton  Reduced 3 hours
Wednesday  2021-01-06  Epiphany / Trettondagen  Office closed
Thursday  2021-04-01  Maundy Thursday / Skärtorsdagen  Reduced 3 hours
Friday  2021-04-02  Good Friday / Långfredagen  Office closed
Monday  2021-04-05  Easter Monday / Annandag påsk  Office closed
Friday  2021-04-30  Walpurgis Night / Valborgsmässoafton  Reduced 3 hours
Thursday  2021-05-13  Ascension Day / Kristi himmelfärds dag  Office closed
Friday  2021-05-14  Bridging Day / Klämdag  Office closed
Friday  2021-06-04  Compensation day / Kompensationsdag  Office closed
Friday  2021-06-25  Midsummer Eve / Midsommarafton  Office closed
Friday  2021-11-05  Eve of all Saints Day / Allhelgonaafton  Reduced 3 hours
Friday  2021-12-24  Christmas Eve / Julafton  Office closed
Friday  2021-12-31  New Year’s Eve / Nyårsafton  Office closed
