"""
This converts the raw copy/paste from Confluence table
"""

year = 2024

fout = open(f"list_{year}.txt", "w")
count = 0
for line in open("orig.txt", "r"):
    if len(line.strip()):
        if "Date" in line:
            fout.write("# ")
        fout.write(line[:-1])
        count += 1
    elif len(line) == 1:
        continue
    else:
        fout.write("  ")
    if count > 3:
        fout.write("\n")
        count = 0
