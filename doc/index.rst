Welcome to ESS Python Tools's documentation!
============================================


Current release: |release|

This is the automatic documentation of the ESS Python tools developed by the beam physics group.

The sources can be found on GitLab_.

.. mdinclude:: ../README.md



.. _GitLab: https://gitlab.esss.lu.se/ess-bp/ess-python-tools/


Contents:

.. toctree::
   :maxdepth: 2

   license
   ess/index
   scripts/index
   examples/index
   developer_info


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Documentation last updated: |today|
