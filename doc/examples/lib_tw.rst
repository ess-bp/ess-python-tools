Lib TraceWin
------------

Some example use cases...

See also the :doc:`API Documentation <../../ess/lib_tw>`

Example 1
=========

Read in plt file, write dst file for each location::

    >>> from ess import lib_tw

    >>> file_name_lat = 'testfiles/lattice.dat'
    >>> file_name_lat_new = 'lattice.adjusted.dat'
    >>> file_name_fmap = [f'testfiles/Field_Maps/{f}' for f in ['Spoke_W_coupler.edz','MB_W_coupler.edz','HB_W_coupler.edz']]
    >>> file_name_st = 'testfiles/Steerer_Values.txt'
    >>> file_name_adj = 'testfiles/Adjusted_Values.txt'

    >>> lat = lib_tw.LATTICE(file_name_lat, file_name_fmap)
    >>> # Updates based on Steerer_Values:
    >>> lat.update_st(file_name_st)
    >>> # Updates based on Adjusted_Values:
    >>> lat.update_adj(file_name_adj)

    >>> lat.get_tw(file_name_lat_new)

    >>> #-- reset all steerers to 0 ...
    >>> for i in range(len(lat.lst)):
    ...     if lat.lst[i].typ=='THIN_STEERING': lat.lst[i].BLx=0.0; lat.lst[i].BLy=0.0
    ...     if lat.lst[i].typ=='STEERER'      : lat.lst[i].Bx =0.0; lat.lst[i].By =0.0



Example 2
=========

lib_tw includes a function to convert loss/elem to loss/elem-length::

    # Note:
    # - For a density file, be careful with the number of runs.
    # - The instances for losses are "loss_pow", "loss_num", "loss_pow_ave", and etc for DENSITY class.
    # - For PARTRAN class, loss_elem2den function is already implemented as a method.
    # - If file_name_dt isn't given, the half-cell lengths are used instead of drift-tube lnegths.
    # - dlt_dt is a small number used to identify DTL_CEL elements. (Play with this number is there is an error message.)
    #
    # - An example to make a bar plot with an output file of this script can be found in
    #   https://gitlab.esss.lu.se/ess-bp/pgfplots-examples/tree/master/loss.den

    from ess import lib_tw

    file_name_den ='density_file_name'
    file_name_ptrn='partran_out_file_name'

    file_name_dt  ='len.dt_dtl.v85.txt'  # File with (half cell length) vs (drift tube length) for DTL cells

    #-- An example for a density file

    den=DENSITY(file_name_den)
    s_from_den   =den.s
    loss_from_den=den.loss_pow         # For a file w/ single run
    #loss_from_den=den.loss_pow[Nrun]  # For a file w/ multi runs (e.g., file from an error study)

    loss_den_from_den=loss_elem2den(s_from_den,loss_from_den,file_name_dt,dtl_dt=5e-6)

    # Note len(s_from_den) = len(loss_den_from_den) when writing to a file.

    #-- An example for a partran out file

    ptrn=PARTRAN(file_name_ptrn)

    loss_den_from_ptrn=ptrn.loss_den(file_name_dt,dtl_dt=5e-6)

    # Note len(ptrn.s) = len(loss_den_from_ptrn) when writing to a file.


Example 3
=========

This example shows how lib_tw can be used to manipulate the lattice

.. literalinclude:: ../../examples/manipulate_lattice/manipulate_lattice.py
