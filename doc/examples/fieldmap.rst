fieldmap
--------

This example shows how fieldmap can be used to modify field map files

.. literalinclude:: ../../examples/fieldmap/fieldmap.py
