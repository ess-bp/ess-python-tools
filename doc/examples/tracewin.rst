TraceWin
--------

This module was developed in parallell with lib_tw, and contains some overlapping functionality. The module allows you to read (and sometimes write) the various binary files from TraceWin into a pythonized object. Some example usecases follows below.

See also the :doc:`API Documentation <../../ess/TraceWin>`

Example 1
=========

Read in plt file, write dst file for each location::

    >>> from ess import TraceWin
    >>> data = TraceWin.plt('testfiles/dtl1.plt')
    >>> for i in data.Nelp:
    ...     dst = data.save_dst(i, f'distribution_{i}.dst')

Example 2
=========

Read in many Density files from a folder, put data into one HDF5 file::

    >>> import os

    >>> calc_dir = 'testfiles'
    >>> for f in os.listdir(calc_dir):
    ...   if f[:7] == 'Density' and f[-3:] == 'dat':
    ...     fpath = os.path.join(calc_dir, f)
    ...     envelope = 'Env' in f
    ...     group_name = f[8:-4]
    ...     data = TraceWin.density(fpath, envelope=envelope)
    ...     data.savetohdf('Density.h5', group_name)

Example 3
=========

Read in the tracewin table, print the length by finding the max z value::

    >>> data = TraceWin.partran('testfiles/tracewin.out')
    >>> print(max(data['z(m)']))
    54.49775
