Developer Information
---------------------

We largely depend on autodocumentation, which allows the documentation on these pages to be in sync and up to date with
what is documented in the source.

For documenting the source, we prefer to use the Numpy style as described in napoleon_.

.. _napoleon: https://www.sphinx-doc.org/en/master/usage/extensions/napoleon.html
