"""
    - Classes for TraceWin elements/commands for external manipulations.
    - First double check all the used elements/commands.

    - Element/command types

      * Active elements

        FIELD_MAP

        DRIFT
        QUAD
        THIN_STEERING
        GAP
        DTL_CEL

      * Passive elements

        BEND
        EDGE
        APERTURE
        DIAG_POSITION
        DIAG_...

      * Active commands

        STEERER
        CHOPPER

      * Passive commands

        ADJUST
        FREQ
        MARKER

      * Error commands (active)

        ERROR_BEAM_STAT
        ERROR_BEAM_DYN
        ERROR_QUAD_NCPL_STAT
        ERROR_QUAD_CPL_STAT
        ERROR_CAV_NCPL_STAT
        ERROR_CAV_NCPL_DYN
        ERROR_CAV_CPL_STAT
        ERROR_CAV_CPL_DYN
        ERROR_STAT_FILE
        ERROR_DYN_FILE

      * Other commands for just common "COMM"

        END
        LATTICE
        LATTICE_END

        SET_ADV
        SET_SIZE
        SET_TWISS
        SET_ACHROMAT
        MATCH_FAM_GRAD
        MATCH_FAM_FIELD
        MATCH_FAM_PHASE
        MATCH_FAM_LFOC
        MIN_PHASE_VARIATION
        START_ACHROMAT

        ADJUST_STEERER
        ADJUST_STEERER_BX
        ADJUST_STEERER_BY
        SET_BEAM_PHASE_ERROR

        PLOT_DST

    - History

      * 2016.01.19

        - Initial ver.

      * 2016.05.07

        - ADJUST activated.
        - max added to THIN_STEERING.

      * 2016.08.18

        - Asymmetric aperture of DRIFT implemented.

      * 2017.01.03

        - ERROR_STAT/DYN_FILE implemented.
"""

# ---- Libs

import numpy

# ---- Constants

# mass=938.272046  # Wiki
mass = 938.272029  # TraceWin
c = 2.99792458

# ---- Misc functions/classes


def Brho(gamma):
    return (10.0 / c) * numpy.sqrt(gamma**2 - 1.0) * mass * 1e-3


class FIELD_MAP_DATA:
    """
    - Only 1D (Ez) supported for now.
    """

    def __init__(self, file_name):
        # Read file
        with open(file_name) as file:
            lin = file.readlines()

        # Instances
        self.dL = float(lin[0].split()[1]) / int(lin[0].split()[0])
        self.field = numpy.array(lin[2:], dtype=float)
        self.file_name = file_name

    def get_bdsim(self, path):
        #  Only support 1D edz for now!
        with open(path + ".txt", "w") as fmapout:
            for i in range(len(self.field)):
                fmapout.write("{} {} {} {}\n".format(self.dL * i, 0.0, 0.0, self.field[i]))
        return '{0}: field, type="ebmap2d", integrator=g4classicalrk4, electricFile=bdsim1d:{0}.txt;'.format(self.file_name[:-4])


# ---- Parent classes


class ELEM:
    """"""

    def __init__(self, name, typ, para):
        # Basic instances
        self.name = name
        self.typ = typ
        self.para = para

        # Option instances
        self.L = 0.0
        self.apt = None
        # idx is essentially similar to "line number"
        self.idx = -1
        # idx_elem is the "tracewin index count" - 1 (we start at 0)
        self.idx_elem = -1
        # For diagnostic elements, the index given in the lattice file:
        self.idx_diag = -1
        self.s = 0.0
        self.gamma = 1.0
        self.freq = 352.21
        self._idx_madx = "0"

    def update_idx(self):
        # Update idx, idx_elem, s
        self.idx += 1
        self.idx_elem += 1
        self._idx_madx = f"{self.idx_elem + 1:04d}"
        self.s += self.L

    def get_tw(self):
        if self.name:
            lin = self.name + ": " + self.typ + " " + " ".join(self.para)
        else:
            lin = self.typ + " " + " ".join(self.para)

        return lin

    # def get_madx(self):

    #     i='0'*(3-int(log10(self.idx_elem+1)))+str(self.idx_elem+1)

    #     if self.name!='': lin= self.name        +': marker;'
    #     if self.name=='': lin='elem'+i+'_marker'+': marker;'

    #     return lin


class COMM:
    """
    - Same inputs as ELEM class but some may not be needed ...
    """

    def __init__(self, name, typ, para):
        # Basic instances
        self.name = name
        self.typ = typ
        self.para = para

        # Option instances
        self.idx = -1
        self.idx_elem = -1
        self.s = 0.0
        self.gamma = 1.0
        self.freq = 352.21

    def update_idx(self):
        # Update idx
        self.idx += 1

    def get_tw(self):
        if self.name != "":
            lin = self.name + ": " + self.typ + " " + " ".join(self.para)
        if self.name == "":
            lin = self.typ + " " + " ".join(self.para)

        return lin


# ---- Field map (need an additional input, dic_fmap)


class FIELD_MAP(ELEM):
    """
    - Calculation of gamma only 1D (Ez) supported.
    """

    def __init__(self, name, typ, para, dic_fmap):
        ELEM.__init__(self, name, typ, para)

        # TW instances
        self.name_fmap = str(para[8])
        self.typ_fmap = str(para[0])
        self.L = float(para[1]) * 1e-3  # mm =>   m
        self.phs_rf = float(para[2]) * numpy.pi / 180.0  # deg => rad
        self.apt = float(para[3])
        self.Bnom = float(para[4])
        self.Enom = float(para[5])
        self.sc_comp = float(para[6])
        self.flag_apt = str(para[7])

        # Option instances
        self.data = None
        if self.name_fmap in dic_fmap:
            self.data = dic_fmap[self.name_fmap]

    def update_gamma(self):
        # Temp error message for field map types
        if self.typ_fmap != "100":
            raise TypeError("Gamma calc only supported for 1D, exiting ...")
        if self.data is None:
            raise AttributeError("A field map file is needed to update gamma calculation")

        # Update gamma (simple ver, closer to TW)
        dL = self.data.dL
        V = self.Enom * numpy.array(self.data.field) * dL
        phs = self.phs_rf
        C = 0.0
        S = 0.0
        for i in range(len(V)):
            C += V[i] * numpy.cos(phs)
            S += V[i] * numpy.sin(phs)
            self.gamma += V[i] / mass * numpy.cos(phs)
            beta = numpy.sqrt(1.0 - 1.0 / self.gamma**2)
            phs += 2.0 * numpy.pi * self.freq * dL / (beta * c * 1e2)

        # Update gamma (I think this is more accurate...)
        # dL=self.data.dL; V=self.Enom*array(self.data.field)*dL; phs=self.phs_rf; C=0; S=0
        # for i in range(len(V)):
        #     if i==0        :
        #         C+=V[i]*cos(phs); S+=V[i]*sin(phs); self.gamma+=0.5*V[i]/mass*cos(phs)
        #         beta=sqrt(1.0-1.0/self.gamma**2); phs+=2.0*pi*self.freq*dL/(beta*c*1e2)
        #     if 0<i<len(V)-1:
        #         C+=V[i]*cos(phs); S+=V[i]*sin(phs); self.gamma+=    V[i]/mass*cos(phs)
        #         beta=sqrt(1.0-1.0/self.gamma**2); phs+=2.0*pi*self.freq*dL/(beta*c*1e2)
        #     if i==len(V)-1 :
        #         C+=V[i]*cos(phs); S+=V[i]*sin(phs); self.gamma+=0.5*V[i]/mass*cos(phs)

        # Option instances for other codes (MADX?)
        self.phs_syn = numpy.arctan(S / C)
        self.E0TL = C / numpy.cos(self.phs_syn)

    def get_tw(self):
        if self.name != "":
            lin = self.name + ": " + self.typ + " "
        if self.name == "":
            lin = self.typ + " "
        lin += str(self.typ_fmap) + " "
        lin += str(self.L * 1e3) + " "
        lin += str(self.phs_rf * 180.0 / numpy.pi) + " "
        lin += str(self.apt) + " "
        lin += str(self.Bnom) + " "
        lin += str(self.Enom) + " "
        lin += str(self.sc_comp) + " "
        lin += str(self.flag_apt) + " "
        lin += str(self.name_fmap)

        return lin

    def get_madx(self):
        if self.name != "":
            lin = self.name + ": RFCAVITY, "
        if self.name == "":
            lin = "ELEM" + self._idx_madx + "_FIELDMAP" + ": RFCAVITY, "
        lin += "L=" + str(self.L) + ", "
        lin += "FREQ=" + str(self.freq) + ", "
        lin += "VOLT=" + str(self.E0TL) + ", "
        lin += "LAG=" + str((0.5 * numpy.pi - self.phs_syn) / (2.0 * numpy.pi)) + "; "  # phs_MADX = pi/2-phs_TW

        return lin

    def get_fluka(self):
        if self.name != "":
            lin = self.name + "  RFCAVITY  "
        if self.name == "":
            lin = "ELEM" + self._idx_madx + "_FIELDMAP" + "  RFCAVITY  "
        lin += str(self.L) + "  " + str(self.s) + "  "
        lin += "0.0  0.0  0.0  "
        lin += "CIRCLE  " + str(self.apt * 1e-3) + "  0.0"

        return lin

    def get_mars(self):
        lin = '"RFCAVITY"  ""  ""  ' + str(self.s) + "  " + str(self.L) + "  "
        lin += "0  0  0  0  0"

        return lin

    def get_bdsim(self):
        return 'element, geometryFile="gdml:{0}.gdml", fieldVacuum="{0}", l={1}'.format(self.name_fmap, self.L)


# ---- Active elements


class DRIFT(ELEM):
    """"""

    def __init__(self, name, typ, para):
        ELEM.__init__(self, name, typ, para)

        # TW instances
        self.L = float(para[0]) * 1e-3  # mm => m
        self.apt = float(para[1])

        # TW option instances
        try:
            self.apty = float(para[2])
        except IndexError:
            self.apty = 0.0
        try:
            self.aptx_cent = float(para[3])
        except IndexError:
            self.aptx_cent = 0.0
        try:
            self.apty_cent = float(para[4])
        except IndexError:
            self.apty_cent = 0.0

    def get_tw(self):
        if self.name != "":
            lin = self.name + ": " + self.typ + " "
        if self.name == "":
            lin = self.typ + " "
        lin += str(self.L * 1e3) + " "
        lin += str(self.apt) + " "
        lin += str(self.apty) + " "
        lin += str(self.aptx_cent) + " "
        lin += str(self.apty_cent)

        return lin

    def get_madx(self):
        if self.name != "":
            lin = self.name + ": DRIFT, L=" + str(self.L) + ";"
        if self.name == "":
            lin = "ELEM" + self._idx_madx + "_DRIFT" + ": DRIFT, L=" + str(self.L) + ";"

        return lin

    def get_fluka(self):
        if self.name != "":
            lin = self.name + "  DRIFT  "
        if self.name == "":
            lin = "ELEM" + self._idx_madx + "_DRIFT" + "  DRIFT  "
        lin += str(self.L) + "  " + str(self.s) + "  "
        lin += "0.0  0.0  0.0  "
        if self.apty == 0.0:
            lin += "CIRCLE  " + str(self.apt * 1e-3) + "  0.0"
        if self.apty != 0.0:
            lin += "RECTANGLE  " + str(self.apt * 1e-3) + "  " + str(self.apty * 1e-3)

        return lin

    def get_mars(self):
        lin = '"DRIFT"  ""  ""  ' + str(self.s) + "  " + str(self.L) + "  "
        lin += "0  0  0  0  0"

        return lin

    def get_bdsim(self):
        return "drift, l={}*mm, beampipeRadius={}*mm;".format(self.L, self.apt)


class QUAD(ELEM):
    """
    - Tilt not supported.
    """

    def __init__(self, name, typ, para):
        ELEM.__init__(self, name, typ, para)

        # TW instances
        self.L = float(para[0]) * 1e-3  # mm => m
        self.G = float(para[1])
        self.apt = float(para[2])
        self.tilt = 0.0

        # TW option instances
        try:
            self.b3 = float(para[4])
        except IndexError:
            self.b3 = 0.0
        try:
            self.b4 = float(para[5])
        except IndexError:
            self.b4 = 0.0
        try:
            self.b5 = float(para[6])
        except IndexError:
            self.b5 = 0.0
        try:
            self.b6 = float(para[7])
        except IndexError:
            self.b6 = 0.0
        try:
            self.r_good = float(para[8])
        except IndexError:
            self.r_good = 0.0

    def get_tw(self):
        if self.name != "":
            lin = self.name + ": " + self.typ + " "
        if self.name == "":
            lin = self.typ + " "
        lin += str(self.L * 1e3) + " "
        lin += str(self.G) + " "
        lin += str(self.apt) + " "
        lin += str(self.tilt) + " "
        lin += str(self.b3) + " "
        lin += str(self.b4) + " "
        lin += str(self.b5) + " "
        lin += str(self.b6) + " "
        lin += str(self.r_good)

        return lin

    def get_madx(self):
        if self.name:
            name = self.name
        else:
            name = f"ELEM{self.idx_elem + 1:04d}_QUAD"
        return f"{name}: QUADRUPOLE, L={self.L}, K1={self.G / Brho(self.gamma)};"

    def get_fluka(self):
        i = "0" * (3 - int(numpy.log10(self.idx_elem + 1))) + str(self.idx_elem + 1)

        if self.name != "":
            lin = self.name + "  QUADRUPOLE  "
        if self.name == "":
            lin = "ELEM" + i + "_QUAD" + "  QUADRUPOLE  "
        lin += str(self.L) + "  " + str(self.s) + "  "
        lin += "0.0  0.0  " + str(self.G * self.L / Brho(self.gamma)) + "  "
        lin += "CIRCLE  " + str(self.apt * 1e-3) + "  0.0"

        return lin

    def get_mars(self):
        lin = '"QUADRUPOLE"  ""  ""  ' + str(self.s) + "  " + str(self.L) + "  "
        lin += "0  " + str(self.G / Brho(self.gamma)) + "  0  0  0  "

        return lin

    def get_bdsim(self):
        return "quadrupole, l={}*mm, k1={}, beampipeRadius={}*mm;".format(self.L, self.G / Brho(self.gamma), self.apt)


class THIN_STEERING(ELEM):
    """
    - Only magnetic steerer supported.
    - Info of plane missing.
    """

    def __init__(self, name, typ, para):
        ELEM.__init__(self, name, typ, para)

        # TW instances
        self.BLx = float(self.para[0])
        self.BLy = float(self.para[1])
        self.apt = float(self.para[2])
        self.typ_EM = "0"

        # Option instances
        self.max = 0.0

    def get_tw(self):
        if self.name != "":
            lin = self.name + ": " + self.typ + " "
        if self.name == "":
            lin = self.typ + " "
        lin += str(self.BLx) + " "
        lin += str(self.BLy) + " "
        lin += str(self.apt) + " "
        lin += str(self.typ_EM)

        return lin

    def get_madx(self):
        i = "0" * (3 - int(numpy.log10(self.idx_elem + 1))) + str(self.idx_elem + 1)

        if self.name != "":
            lin = self.name + ": KICKER, L=0, "
        if self.name == "":
            lin = "ELEM" + i + "_STEERER" + ": KICKER, L=0, "
        lin += "HKICK=" + str(-self.BLy / Brho(self.gamma)) + ", "
        lin += "VKICK=" + str(self.BLx / Brho(self.gamma)) + "; "

        return lin

    def get_fluka(self):
        i = "0" * (3 - int(numpy.log10(self.idx_elem + 1))) + str(self.idx_elem + 1)

        if self.name != "":
            lin = self.name + "  KICKER  "
        if self.name == "":
            lin = "ELEM" + i + "_STEERER" + "  KICKER  "
        lin += str(self.L) + "  " + str(self.s) + "  "
        lin += "0.0  0.0  0.0  "
        lin += "CIRCLE  " + str(self.apt * 1e-3) + "  0.0"

        return lin

    def get_mars(self):
        lin = '"KICKER"  ""  ""  ' + str(self.s) + "  " + str(self.L) + "  "
        lin += "0  0  0  0  0"

        return lin


class GAP(ELEM):
    """
    - Note there are E0T0L (for TW) and E0TL (for other codes).
    """

    def __init__(self, name, typ, para):
        ELEM.__init__(self, name, typ, para)

        # TW instances
        self.E0T0L = float(para[0]) * 1e-6  # V => MV
        self.phs_rf = float(para[1]) * numpy.pi / 180.0  # deg => rad
        self.apt = float(para[2])
        self.phs_rf_typ = int(para[3])

        # TW option instances
        try:
            self.beta_s = float(para[4])
        except IndexError:
            self.beta_s = 0.0
        try:
            self.T0 = float(para[5])
        except IndexError:
            self.T0 = 0.0
        try:
            self.T1 = float(para[6])
        except IndexError:
            self.T1 = 0.0
        try:
            self.T2 = float(para[7])
        except IndexError:
            self.T2 = 0.0

        # Option instances
        self.E0TL = self.E0T0L

    def update_gamma(self):
        # Update gamma (and E0TL)
        if self.beta_s != 0.0 and self.T0 != 0.0:
            gamma_c = self.gamma + 0.5 * self.E0TL / mass * numpy.cos(self.phs_rf)
            beta_c = numpy.sqrt(1.0 - 1.0 / gamma_c**2)
            k = self.beta_s / beta_c
            self.E0TL *= (self.T0 + self.T1 * (k - 1.0) + 0.5 * self.T2 * (k - 1.0) ** 2) / self.T0
        self.gamma += self.E0TL / mass * numpy.cos(self.phs_rf)

    def get_tw(self):
        if self.name != "":
            lin = self.name + ": " + self.typ + " "
        if self.name == "":
            lin = self.typ + " "
        lin += str(self.E0T0L * 1e6) + " "
        lin += str(self.phs_rf * 180.0 / numpy.pi) + " "
        lin += str(self.apt) + " "
        lin += str(self.phs_rf_typ) + " "
        lin += str(self.beta_s) + " "
        lin += str(self.T0) + " "
        lin += str(self.T1) + " "
        lin += str(self.T2)

        return lin

    def get_madx(self):
        name = self.name if self.name else f"ELEM{self.idx_elem + 1:04d}_GAP"
        lag = (0.5 * numpy.pi - self.phs_rf) / (2.0 * numpy.pi)  # phs_MADX = pi/2-phs_TW

        return f"{name}: RFCAVITY, L=0, FREQ={self.freq}, VOLT={self.E0TL}, LAG={lag}"

    def get_fluka(self):
        i = "0" * (3 - int(numpy.log10(self.idx_elem + 1))) + str(self.idx_elem + 1)

        if self.name != "":
            lin = self.name + "  RFCAVITY  "
        if self.name == "":
            lin = "ELEM" + i + "_GAP" + "  RFCAVITY  "
        lin += str(self.L) + "  " + str(self.s) + "  "
        lin += "0.0  0.0  0.0  "
        lin += "CIRCLE  " + str(self.apt * 1e-3) + "  0.0"

        return lin

    def get_mars(self):
        lin = '"RFCAVITY"  ""  ""  ' + str(self.s) + "  " + str(self.L) + "  "
        lin += "0  0  0  0  0"

        return lin


class DTL_CEL(ELEM):
    """
    - Note there are E0T0L (for TW) and E0TL (for other codes).
    """

    def __init__(self, name, typ, para):
        ELEM.__init__(self, name, typ, para)

        # TW instances
        self.L = float(para[0]) * 1e-3  # mm => m
        self.L_Q1 = float(para[1]) * 1e-3  # mm => m
        self.L_Q2 = float(para[2]) * 1e-3  # mm => m
        self.ds_gap = float(para[3]) * 1e-3  # mm => m
        self.G_Q1 = float(para[4])
        self.G_Q2 = float(para[5])
        self.E0T0L = float(para[6]) * 1e-6  # V => MV
        self.phs_rf = float(para[7]) * numpy.pi / 180.0  # deg => rad
        self.apt = float(para[8])
        self.phs_rf_typ = int(float(para[9]))

        # TW option instances
        try:
            self.beta_s = float(para[10])
        except IndexError:
            self.beta_s = 0.0
        try:
            self.T0 = float(para[11])
        except IndexError:
            self.T0 = 0.0
        try:
            self.T1 = float(para[12])
        except IndexError:
            self.T1 = 0.0
        try:
            self.T2 = float(para[13])
        except IndexError:
            self.T2 = 0.0

        # Option instances
        self.E0TL = self.E0T0L

    def update_gamma(self):
        # Save the gamma_ini (for MADX)
        self.gamma_ini = self.gamma

        # Update gamma (and E0TL)
        if self.beta_s != 0.0 and self.T0 != 0.0:
            gamma_c = self.gamma + 0.5 * self.E0TL / mass * numpy.cos(self.phs_rf)
            beta_c = numpy.sqrt(1.0 - 1.0 / gamma_c**2)
            k = self.beta_s / beta_c
            self.E0TL *= (self.T0 + self.T1 * (k - 1.0) + 0.5 * self.T2 * (k - 1.0) ** 2) / self.T0
        self.gamma += self.E0TL / mass * numpy.cos(self.phs_rf)

    def get_tw(self):
        if self.name != "":
            lin = self.name + ": " + self.typ + " "
        if self.name == "":
            lin = self.typ + " "
        lin += str(self.L * 1e3) + " "
        lin += str(self.L_Q1 * 1e3) + " "
        lin += str(self.L_Q2 * 1e3) + " "
        lin += str(self.ds_gap * 1e3) + " "
        lin += str(self.G_Q1) + " "
        lin += str(self.G_Q2) + " "
        lin += str(self.E0T0L * 1e6) + " "
        lin += str(self.phs_rf * 180.0 / numpy.pi) + " "
        lin += str(self.apt) + " "
        lin += str(self.phs_rf_typ) + " "
        lin += str(self.beta_s) + " "
        lin += str(self.T0) + " "
        lin += str(self.T1) + " "
        lin += str(self.T2)

        return lin

    def get_madx(self):
        i = "0" * (3 - int(numpy.log10(self.idx_elem + 1))) + str(self.idx_elem + 1)

        if self.name != "":
            lin = self.name + "_QUAD1: QUADRUPOLE, "
        if self.name == "":
            lin = "ELEM" + i + "_DTLCELL" + "_QUAD1: QUADRUPOLE, "
        lin += "L=" + str(self.L_Q1) + ", "
        lin += "K1=" + str(self.G_Q1 / Brho(self.gamma_ini)) + "; \n"

        if self.name != "":
            lin += self.name + "_DRIFT1: DRIFT, "
        if self.name == "":
            lin += "ELEM" + i + "_DTLCELL" + "_DRIFT1: DRIFT, "
        lin += "L=" + str(0.5 * self.L - self.L_Q1 - self.ds_gap) + ";\n"

        if self.name != "":
            lin += self.name + "_GAP: RFCAVITY, L=0, "
        if self.name == "":
            lin += "ELEM" + i + "_DTLCELL" + "_GAP: RFCAVITY, L=0, "
        lin += "FREQ=" + str(self.freq) + ", "
        lin += "VOLT=" + str(self.E0TL) + ", "
        lin += "LAG=" + str((0.5 * numpy.pi - self.phs_rf) / (2.0 * numpy.pi)) + "; \n"  # phs_MADX = pi/2-phs_TW

        if self.name != "":
            lin += self.name + "_DRIFT2: DRIFT, "
        if self.name == "":
            lin += "ELEM" + i + "_DTLCELL" + "_DRIFT2: DRIFT, "
        lin += "L=" + str(0.5 * self.L - self.L_Q2 + self.ds_gap) + ";\n"

        if self.name != "":
            lin += self.name + "_QUAD2: QUADRUPOLE, "
        if self.name == "":
            lin += "ELEM" + i + "_DTLCELL" + "_QUAD2: QUADRUPOLE, "
        lin += "L=" + str(self.L_Q2) + ", "
        lin += "K1=" + str(self.G_Q2 / Brho(self.gamma)) + "; "

        return lin

    def get_fluka(self):
        i = "0" * (3 - int(numpy.log10(self.idx_elem + 1))) + str(self.idx_elem + 1)

        # if self.name!='': lin= self.name         +'_QUAD1  QUADRUPOLE  '
        # if self.name=='': lin='ELEM'+i+'_DTLCELL'+'_QUAD1  QUADRUPOLE  '
        # lin+=str(self.L_Q1)+'  '+str(self.s-self.L+self.L_Q1)+'  '
        # lin+='0.0  0.0  '+str(self.G_Q1*self.L_Q1/Brho(self.gamma_ini))+'  '
        # lin+='CIRCLE  '+str(self.apt*1e-3)+'  0.0\n'

        # if self.name!='': lin+= self.name         +'_DRIFT1  DRIFT  '
        # if self.name=='': lin+='ELEM'+i+'_DTLCELL'+'_DRIFT1  DRIFT  '
        # lin+=str(0.5*self.L-self.L_Q1-self.ds_gap)+'  '+str(self.s-0.5*self.L-self.ds_gap)+'  '
        # lin+='0.0  0.0  0.0  '
        # lin+='CIRCLE  '+str(self.apt*1e-3)+'  0.0\n'

        # if self.name!='': lin+= self.name         +'_GAP  RFCAVITY   '
        # if self.name=='': lin+='ELEM'+i+'_DTLCELL'+'_GAP  RFCAVITY   '
        # lin+='0.0  '+str(self.s-0.5*self.L-self.ds_gap)+'  '
        # lin+='0.0  0.0  0.0  '
        # lin+='CIRCLE  '+str(self.apt*1e-3)+'  0.0\n'

        # if self.name!='': lin+= self.name         +'_DRIFT2  DRIFT  '
        # if self.name=='': lin+='ELEM'+i+'_DTLCELL'+'_DRIFT2  DRIFT  '
        # lin+=str(0.5*self.L-self.L_Q2+self.ds_gap)+'  '+str(self.s-self.L_Q2)+'  '
        # lin+='0.0  0.0  0.0  '
        # lin+='CIRCLE  '+str(self.apt*1e-3)+'  0.0\n'

        # if self.name!='': lin+= self.name         +'_QUAD2  QUADRUPOLE  '
        # if self.name=='': lin+='ELEM'+i+'_DTLCELL'+'_QUAD2  QUADRUPOLE  '
        # lin+=str(self.L_Q2)+'  '+str(self.s)+'  '
        # lin+='0.0  0.0  '+str(self.G_Q2*self.L_Q2/Brho(self.gamma))+'  '
        # lin+='CIRCLE  '+str(self.apt*1e-3)+'  0.0'

        if self.name != "":
            lin = self.name + "  RFCAVITY  "
        if self.name == "":
            lin = "ELEM" + i + "_DTLCELL" + "  RFCAVITY  "
        lin += str(self.L) + "  " + str(self.s) + "  "
        lin += "0.0  0.0  0.0  "
        lin += "CIRCLE  " + str(self.apt * 1e-3) + "  0.0"

        return lin

    def get_mars(self):
        lin = '"QUADRUPOLE"  ""  ""  '
        lin += str(self.s - self.L + self.L_Q1) + "  "
        lin += str(self.L_Q1) + "  "
        lin += "0  " + str(self.G_Q1 / Brho(self.gamma_ini)) + "  0  0  0  \n"

        lin += '"DRIFT"  ""  ""  '
        lin += str(self.s - 0.5 * self.L - self.ds_gap) + "  "
        lin += str(0.5 * self.L - self.L_Q1 - self.ds_gap) + "  "
        lin += "0  0  0  0  0  \n"

        lin += '"RFCAVITY"  ""  ""  '
        lin += str(self.s - 0.5 * self.L - self.ds_gap) + "  "
        lin += "0  "
        lin += "0  0  0  0  0  \n"

        lin += '"DRIFT"  ""  ""  '
        lin += str(self.s - self.L_Q2) + "  "
        lin += str(0.5 * self.L - self.L_Q2 + self.ds_gap) + "  "
        lin += "0  0  0  0  0  \n"

        lin += '"QUADRUPOLE"  ""  ""  '
        lin += str(self.s) + "  "
        lin += str(self.L_Q2) + "  "
        lin += "0  " + str(self.G_Q2 / Brho(self.gamma)) + "  0  0  0  "

        return lin


# ---- Passive elements


class BEND(ELEM):
    """
    - For MADX, keeping the sbend+edge structure.
    - For MADX, if don't export edge, append instances of EDGE.
      (Otherwise, the matrix is wrong in MADX.)
    - For MARS/Fluka, assuming the rbend.
    """

    def __init__(self, name, typ, para):
        ELEM.__init__(self, name, typ, para)

        # TW instances
        self.angle = float(para[0]) * numpy.pi / 180.0  # deg => rad
        self.rho = float(para[1]) * 1e-3  # mm => m
        self.apt = float(para[3])
        self.plane = str(para[4])

        # Option instances
        if self.plane == "0":
            self.tilt = 0.0
        if self.plane == "1":
            self.tilt = 0.5 * numpy.pi
        self.ds = self.rho * abs(self.angle)
        self.L = 2.0 * self.rho * numpy.sin(0.5 * abs(self.angle))
        self.gap = 0.0  # Get this from EDGE IF not exporting edge for MADX
        self.ff_coef1 = 0.0  # Get this from EDGE IF not exporting edge for MADX
        self.ff_coef2 = 0.0  # Get this from EDGE IF not exporting edge for MADX

    def update_idx(self):
        # Updata idx, idx_elem, s
        self.idx += 1
        self.idx_elem += 1
        self.s += self.ds  # Need an overwrite because of this

    def get_madx(self):
        i = "0" * (3 - int(numpy.log10(self.idx_elem + 1))) + str(self.idx_elem + 1)

        if self.name != "":
            lin = self.name + ": "
        if self.name == "":
            lin = "ELEM" + i + "_BEND" + ": "
        lin += "SBEND, L=" + str(self.ds) + ", "  # sbend
        # lin+='RBEND, L='+str(self.L       )+', '  # rbend
        lin += "ANGLE=" + str(self.angle) + ", "
        lin += "TILT=" + str(self.tilt) + ", "
        lin += "HGAP=" + str(self.gap * 5e-4) + ", "
        lin += "FINT=" + str(self.ff_coef1) + "; "

        return lin

    def get_fluka(self):
        i = "0" * (3 - int(numpy.log10(self.idx_elem + 1))) + str(self.idx_elem + 1)

        if self.name != "":
            lin = self.name + "  RBEND  "
        if self.name == "":
            lin = "ELEM" + i + "_BEND" + "  RBEND  "
        lin += str(self.ds) + "  " + str(self.s) + "  "
        lin += str(self.tilt) + "  " + str(self.angle) + "  0.0  "
        lin += "CIRCLE  " + str(self.apt * 1e-3) + "  0.0"

        return lin

    def get_mars(self):
        lin = '"RBEND"  ""  ""  ' + str(self.s) + "  " + str(self.L) + "  "
        lin += str(self.angle) + "  0  0  0  " + str(self.tilt)

        return lin


class EDGE(ELEM):
    """
    - For MARS, assuming rbend and the export is commented out.
    """

    def __init__(self, name, typ, para):
        ELEM.__init__(self, name, typ, para)

        # TW instances
        self.angle = float(para[0]) * numpy.pi / 180.0  # deg => rad
        self.rho = float(para[1]) * 1e-3  # mm => m
        self.gap = float(para[2])
        self.ff_coef1 = float(para[3])
        self.ff_coef2 = float(para[4])
        self.apt = float(para[5])
        self.plane = str(para[6])

        # Option instances
        if self.plane == "0":
            self.tilt = 0.0
        if self.plane == "1":
            self.tilt = 0.5 * numpy.pi

    def get_madx(self):
        i = "0" * (3 - int(numpy.log10(self.idx_elem + 1))) + str(self.idx_elem + 1)

        if self.name != "":
            lin = self.name + ": DIPEDGE, "
        if self.name == "":
            lin = "ELEM" + i + "_EDGE" + ": DIPEDGE, "
        lin += "E1=" + str(self.angle) + ", "  # sbend
        # lin+='E1='  +'0'               +', '  # rbend
        lin += "H=" + str(1.0 / self.rho) + ", "
        lin += "TILT=" + str(self.tilt) + ", "
        lin += "HGAP=" + str(self.gap * 5e-4) + ", "
        lin += "FINT=" + str(self.ff_coef1) + "; "

        return lin


class APERTURE(ELEM):
    """
    - For MADX, tried "collimator" but didn't work ...?
    """

    def __init__(self, name, typ, para):
        ELEM.__init__(self, name, typ, para)

        # TW instances
        self.apt = float(para[0])
        self.apty = float(para[1])
        self.typ_apt = str(para[2])

    def get_madx(self):
        i = "0" * (3 - int(numpy.log10(self.idx_elem + 1))) + str(self.idx_elem + 1)

        if self.name != "":
            lin = self.name + ": DRIFT, L=0;"
        if self.name == "":
            lin = "ELEM" + i + "_APT" + ": DRIFT, L=0;"

        # if self.name!='': lin= self.name     +': collimator, l=0, '
        # if self.name=='': lin='ELEM'+i+'_apt'+': collimator, l=0, '
        # if self.typ_apt=='0':
        #     lin+='apertype=rectangle, aperture={'+str(self.apt*1e-3)+','+str(self.apty*1e-3)+'};'
        # if self.typ_apt=='1':
        #     lin+='apertype=CIRCLE, aperture={'+str(self.apt*1e-3)+'};'

        return lin

    def get_fluka(self):
        i = "0" * (3 - int(numpy.log10(self.idx_elem + 1))) + str(self.idx_elem + 1)

        if self.name != "":
            lin = self.name + "  DRIFT  "
        if self.name == "":
            lin = "ELEM" + i + "_APT" + "  DRIFT  "
        lin += str(self.L) + "  " + str(self.s) + "  "
        lin += "0.0  0.0  0.0  "
        if self.typ_apt == "0":
            lin += "RECTANGLE  " + str(self.apt * 1e-3) + "  " + str(self.apty * 1e-3)
        if self.typ_apt == "1":
            lin += "CIRCLE  " + str(self.apt * 1e-3) + "  0.0"

        return lin

    def get_mars(self):
        lin = '"drift"  ""  ""  ' + str(self.s) + "  " + str(self.L) + "  "
        lin += "0  0  0  0  0"

        return lin


class DIAG(ELEM):
    """"""

    def __init__(self, name, typ, para):
        ELEM.__init__(self, name, typ, para)
        self.idx_diag = int(para[0])
        self.__diag_type = "INSTRUMENT"
        self.__diag_name = "DIAG"

    def get_madx(self):
        if self.name != "":
            lin = f"{self.name}: {self.__diag_type}, L=0;"
        if self.name == "":
            lin = f"ELEM{self._idx_madx}_{self.__diag_name}: {self.__diag_type}, L=0;"

        return lin

    def get_fluka(self):
        if self.name != "":
            lin = f"{self.name} {self.__diag_type}  "
        if self.name == "":
            lin = f"ELEM{self._idx_madx}_{self.__diag_name} {self.__diag_type}  "
        lin += f"{self.L} {self.s} 0.0  0.0  0.0  "
        lin += f"CIRCLE  {self.apt * 1e-3} 0.0"

        return lin

    def get_mars(self):
        lin = f'"{self.__diag_type}" "" "" {self.s} {self.L} 0  0  0  0  0'

        return lin


class DIAG_POSITION(DIAG):
    """"""

    def __init__(self, name, typ, para):
        DIAG.__init__(self, name, typ, para)
        self.__diag_type = "MONITOR"
        self.__diag_name = "BPM"


# ---- Active commands


class STEERER(COMM):
    """
    - Only magnetic steerers supported.
    - Only the ESS type non-linearity supported.
    """

    def __init__(self, name, typ, para):
        COMM.__init__(self, name, typ, para)

        # TW instances
        self.Bx = float(para[0])
        self.By = float(para[1])

        # TW option instances
        try:
            self.max = float(para[2])
        except IndexError:
            self.max = 0.0
        try:
            self.nonlin = float(para[5])
        except IndexError:
            self.nonlin = 0.0

    def get_tw(self):
        if self.name != "":
            lin = self.name + ": " + self.typ + " "
        if self.name == "":
            lin = self.typ + " "
        lin += str(self.Bx) + " "
        lin += str(self.By) + " "
        lin += str(self.max) + " "
        lin += "0" + " "
        lin += "0" + " "
        lin += str(self.nonlin)

        return lin


class CHOPPER(COMM):
    """"""

    def __init__(self, name, typ, para):
        COMM.__init__(self, name, typ, para)

        # TW instances
        self.Nelem = int(para[0])
        self.volt = float(para[1])
        self.gap = float(para[2]) * 2.0
        self.gap_cent = float(para[3])
        self.plane = str(para[4])

    def get_tw(self):
        if self.name != "":
            lin = self.name + ": " + self.typ + " "
        if self.name == "":
            lin = self.typ + " "
        lin += str(self.Nelem) + " "
        lin += str(self.volt) + " "
        lin += str(self.gap * 0.5) + " "
        lin += str(self.gap_cent) + " "
        lin += str(self.plane)

        return lin


# ---- Passive commands


class FREQ(COMM):
    """"""

    def __init__(self, name, typ, para):
        COMM.__init__(self, name, typ, para)

        # TW instances
        self.freq_new = float(para[0])

    def update_idx(self):
        self.idx += 1
        self.freq = self.freq_new


class MARKER(COMM):
    """"""

    def __init__(self, name, typ, para):
        COMM.__init__(self, name, typ, para)

    def get_madx(self):
        if self.name != "":
            lin = self.name + ": MARKER;"
        if self.name == "":
            lin = self.para[0] + ": MARKER;"

        return lin

    def get_fluka(self):
        if self.name != "":
            lin = self.name + "  MARKER  "
        if self.name == "":
            lin = self.para[0] + "  MARKER  "
        lin += "0.0  " + str(self.s)

        return lin

    # def get_mars(self):

    #     lin ='"marker"  ""  ""  '+str(self.s)+'  0.0  '
    #     lin+='0  0  0  0  0'

    #     return lin


class ADJUST(COMM):
    """"""

    def __init__(self, name, typ, para):
        COMM.__init__(self, name, typ, para)

        # TW instances
        self.fam = str(para[0])
        self.var = int(para[1])

        # TW option instances
        try:
            self.link = str(self.para[2])
        except IndexError:
            self.link = "0"
        try:
            self.min = float(self.para[3])
        except IndexError:
            self.min = 0.0
        try:
            self.max = float(self.para[4])
        except IndexError:
            self.max = 0.0
        try:
            self.ini = float(self.para[5])
        except IndexError:
            self.ini = 0.0


# ---- Error commands (active)

# --


class ERROR_BEAM(COMM):
    """"""

    def __init__(self, name, typ, para):
        COMM.__init__(self, name, typ, para)

        # TW instances
        self.typ_dist = str(para[0])

        # TW option instances
        try:
            self.x = float(para[1])
        except IndexError:
            self.x = 0.0
        try:
            self.y = float(para[2])
        except IndexError:
            self.y = 0.0
        try:
            self.phs = float(para[3]) * numpy.pi / 180.0  # deg => rad
        except IndexError:
            self.phs = 0.0
        try:
            self.xp = float(para[4])
        except IndexError:
            self.xp = 0.0
        try:
            self.yp = float(para[5])
        except IndexError:
            self.yp = 0.0
        try:
            self.ken = float(para[6])
        except IndexError:
            self.ken = 0.0
        try:
            self.epsx = float(para[7]) * 1e-2  # % => 1
        except IndexError:
            self.epsx = 0.0
        try:
            self.epsy = float(para[8]) * 1e-2  # % => 1
        except IndexError:
            self.epsy = 0.0
        try:
            self.epsz = float(para[9]) * 1e-2  # % => 1
        except IndexError:
            self.epsz = 0.0
        try:
            self.mx = float(para[10]) * 1e-2  # % => 1
        except IndexError:
            self.mx = 0.0
        try:
            self.my = float(para[11]) * 1e-2  # % => 1
        except IndexError:
            self.my = 0.0
        try:
            self.mz = float(para[12]) * 1e-2  # % => 1
        except IndexError:
            self.mz = 0.0
        try:
            self.Ibeam = float(para[13])
        except IndexError:
            self.Ibeam = 0.0
        try:
            self.alfx_min = float(para[14])
        except IndexError:
            self.alfx_min = 0.0
        try:
            self.alfx_max = float(para[15])
        except IndexError:
            self.alfx_max = 0.0
        try:
            self.betx_min = float(para[16])
        except IndexError:
            self.betx_min = 0.0
        try:
            self.betx_max = float(para[17])
        except IndexError:
            self.betx_max = 0.0
        try:
            self.alfy_min = float(para[18])
        except IndexError:
            self.alfy_min = 0.0
        try:
            self.alfy_max = float(para[19])
        except IndexError:
            self.alfy_max = 0.0
        try:
            self.bety_min = float(para[20])
        except IndexError:
            self.bety_min = 0.0
        try:
            self.bety_max = float(para[21])
        except IndexError:
            self.bety_max = 0.0
        try:
            self.alfz_min = float(para[22])
        except IndexError:
            self.alfz_min = 0.0
        try:
            self.alfz_max = float(para[23])
        except IndexError:
            self.alfz_max = 0.0
        try:
            self.betz_min = float(para[24])
        except IndexError:
            self.betz_min = 0.0
        try:
            self.betz_max = float(para[25])
        except IndexError:
            self.betz_max = 0.0

    def get_tw(self):
        if self.name != "":
            lin = self.name + ": " + self.typ + " "
        if self.name == "":
            lin = self.typ + " "
        lin += str(self.typ_dist) + " "
        lin += str(self.x) + " "
        lin += str(self.y) + " "
        lin += str(self.phs * 180.0 / numpy.pi) + " "
        lin += str(self.xp) + " "
        lin += str(self.yp) + " "
        lin += str(self.ken) + " "
        lin += str(self.epsx * 1e2) + " "
        lin += str(self.epsy * 1e2) + " "
        lin += str(self.epsz * 1e2) + " "
        lin += str(self.mx * 1e2) + " "
        lin += str(self.my * 1e2) + " "
        lin += str(self.mz * 1e2) + " "
        lin += str(self.Ibeam) + " "
        lin += str(self.alfx_min) + " "
        lin += str(self.alfx_max) + " "
        lin += str(self.betx_min) + " "
        lin += str(self.betx_max) + " "
        lin += str(self.alfy_min) + " "
        lin += str(self.alfy_max) + " "
        lin += str(self.bety_min) + " "
        lin += str(self.bety_max) + " "
        lin += str(self.alfz_min) + " "
        lin += str(self.alfz_max) + " "
        lin += str(self.betz_min) + " "
        lin += str(self.betz_max)

        return lin


class ERROR_BEAM_STAT(ERROR_BEAM):
    """"""

    def __init__(self, name, typ, para):
        ERROR_BEAM.__init__(self, name, typ, para)


class ERROR_BEAM_DYN(ERROR_BEAM):
    """"""

    def __init__(self, name, typ, para):
        ERROR_BEAM.__init__(self, name, typ, para)


# --


class ERROR_QUAD(COMM):
    """"""

    def __init__(self, name, typ, para):
        COMM.__init__(self, name, typ, para)

        # TW instances
        self.Nelem = int(para[0])
        self.typ_dist = str(para[1])

        # TW option instances
        try:
            self.x = float(para[2])
        except IndexError:
            self.x = 0.0
        try:
            self.y = float(para[3])
        except IndexError:
            self.y = 0.0
        try:
            self.rotx = float(para[4]) * numpy.pi / 180.0  # deg => rad
        except IndexError:
            self.rotx = 0.0
        try:
            self.roty = float(para[5]) * numpy.pi / 180.0  # deg => rad
        except IndexError:
            self.roty = 0.0
        try:
            self.rotz = float(para[6]) * numpy.pi / 180.0  # deg => rad
        except IndexError:
            self.rotz = 0.0
        try:
            self.G = float(para[7]) * 1e-2  # % => 1
        except IndexError:
            self.G = 0.0
        try:
            self.z = float(para[8])
        except IndexError:
            self.z = 0.0
        try:
            self.b3 = float(para[9]) * 1e-2  # % => 1
        except IndexError:
            self.b3 = 0.0
        try:
            self.b4 = float(para[10]) * 1e-2  # % => 1
        except IndexError:
            self.b4 = 0.0
        try:
            self.b5 = float(para[11]) * 1e-2  # % => 1
        except IndexError:
            self.b5 = 0.0
        try:
            self.b6 = float(para[12]) * 1e-2  # % => 1
        except IndexError:
            self.b6 = 0.0

    def get_tw(self):
        if self.name != "":
            lin = self.name + ": " + self.typ + " "
        if self.name == "":
            lin = self.typ + " "
        lin += str(self.Nelem) + " "
        lin += str(self.typ_dist) + " "
        lin += str(self.x) + " "
        lin += str(self.y) + " "
        lin += str(self.rotx * 180.0 / numpy.pi) + " "
        lin += str(self.roty * 180.0 / numpy.pi) + " "
        lin += str(self.rotz * 180.0 / numpy.pi) + " "
        lin += str(self.G * 1e2) + " "
        lin += str(self.z) + " "
        lin += str(self.b3 * 1e2) + " "
        lin += str(self.b4 * 1e2) + " "
        lin += str(self.b5 * 1e2) + " "
        lin += str(self.b6 * 1e2)

        return lin


class ERROR_QUAD_NCPL_STAT(ERROR_QUAD):
    """"""

    def __init__(self, name, typ, para):
        ERROR_QUAD.__init__(self, name, typ, para)


class ERROR_QUAD_CPL_STAT(ERROR_QUAD):
    """"""

    def __init__(self, name, typ, para):
        ERROR_QUAD.__init__(self, name, typ, para)


# --


class ERROR_CAV(COMM):
    """"""

    def __init__(self, name, typ, para):
        COMM.__init__(self, name, typ, para)

        # TW instances
        self.Nelem = int(para[0])
        self.typ_dist = str(para[1])

        # TW option instances
        try:
            self.x = float(para[2])
        except IndexError:
            self.x = 0.0
        try:
            self.y = float(para[3])
        except IndexError:
            self.y = 0.0
        try:
            self.rotx = float(para[4]) * numpy.pi / 180.0  # deg => rad
        except IndexError:
            self.rotx = 0.0
        try:
            self.roty = float(para[5]) * numpy.pi / 180.0  # deg => rad
        except IndexError:
            self.roty = 0.0
        try:
            self.E = float(para[6]) * 1e-2  # % =>   1
        except IndexError:
            self.E = 0.0
        try:
            self.phs_rf = float(para[7]) * numpy.pi / 180.0  # deg => rad
        except IndexError:
            self.phs_rf = 0.0
        try:
            self.z = float(para[8])
        except IndexError:
            self.z = 0.0

    def get_tw(self):
        if self.name != "":
            lin = self.name + ": " + self.typ + " "
        if self.name == "":
            lin = self.typ + " "
        lin += str(self.Nelem) + " "
        lin += str(self.typ_dist) + " "
        lin += str(self.x) + " "
        lin += str(self.y) + " "
        lin += str(self.rotx * 180.0 / numpy.pi) + " "
        lin += str(self.roty * 180.0 / numpy.pi) + " "
        lin += str(self.E * 1e2) + " "
        lin += str(self.phs_rf * 180.0 / numpy.pi) + " "
        lin += str(self.z)

        return lin


class ERROR_CAV_NCPL_STAT(ERROR_CAV):
    """"""

    def __init__(self, name, typ, para):
        ERROR_CAV.__init__(self, name, typ, para)


class ERROR_CAV_CPL_STAT(ERROR_CAV):
    """"""

    def __init__(self, name, typ, para):
        ERROR_CAV.__init__(self, name, typ, para)


class ERROR_CAV_NCPL_DYN(ERROR_CAV):
    """"""

    def __init__(self, name, typ, para):
        ERROR_CAV.__init__(self, name, typ, para)


class ERROR_CAV_CPL_DYN(ERROR_CAV):
    """"""

    def __init__(self, name, typ, para):
        ERROR_CAV.__init__(self, name, typ, para)


# --


class ERROR_FILE(COMM):
    """"""

    def __init__(self, name, typ, para):
        COMM.__init__(self, name, typ, para)

        # TW instances
        self.file_name = str(para[0])

    def get_tw(self):
        if self.name != "":
            lin = self.name + ": " + self.typ + " "
        if self.name == "":
            lin = self.typ + " "
        lin += str(self.file_name)

        return lin


class ERROR_STAT_FILE(ERROR_FILE):
    """"""

    def __init__(self, name, typ, para):
        ERROR_FILE.__init__(self, name, typ, para)


class ERROR_DYN_FILE(ERROR_FILE):
    """"""

    def __init__(self, name, typ, para):
        ERROR_FILE.__init__(self, name, typ, para)


# ----
