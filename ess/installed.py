def get_installed_packages():
    """
    Returns a dictionary of installed packages

    Example usage:
    (print all packages):
    for package in get_installed_packages():
        print package
    """
    import pip

    installed_packages = pip.get_installed_distributions()
    return installed_packages


def is_package_installed(name):
    """
    Returns a bool, true if package name
    was found in one of the string representations
    of any package.

    case insensitive (meaning Package==package)
    """
    name = name.lower()
    ret = False
    for package in get_installed_packages():
        if name in str(package).lower():
            print("Found", package)
            ret = True
    return ret
