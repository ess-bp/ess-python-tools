# supporting templates..

HEAD_TEMPLATE = """
universe       = Vanilla
# always transfer files to tmp directory
should_transfer_files = yes
initialdir      = {args.calc_dir}/Local_TraceWin_$(Process)
transfer_input_files = {input_files}
requirements = ( OpSys == "LINUX" || OpSys == "OSX" )
log            = condor.log
executable     = $ENV(HOME)/TraceWin/exe/tracewin.$$(OpSys).$$(Arch)
"""
HEAD_MULTI_TEMPLATE = """
universe       = Vanilla
# always transfer files to tmp directory
should_transfer_files = yes
requirements = ( OpSys == "LINUX" || OpSys == "OSX" )
log            = condor.log
executable     = TraceWin/exe/tracewin.$$(OpSys).$$(Arch)
"""

QUEUE_TEMPLATE = """
priority = {q_priority}
arguments      = 1 {stat_study} {rand}
queue 1
"""

QUEUE_MULTI_TEMPLATE = """
initialdir      = {args.calc_dir}/{job_folder}
transfer_input_files = {input_files}
priority = {q_priority}
arguments      = 1 {stat_study} {rand}
queue 1
"""


def get_head_template(filename="head.tmp"):
    return _get_template(filename, HEAD_TEMPLATE)


def get_head_multi_template(filename="head.multi.tmp"):
    return _get_template(filename, HEAD_MULTI_TEMPLATE)


def get_queue_template(filename="queue.tmp"):
    return _get_template(filename, QUEUE_TEMPLATE)


def get_queue_multi_template(filename="queue.multi.tmp"):
    return _get_template(filename, QUEUE_MULTI_TEMPLATE)


def _get_template(filename, template):
    """
    Only to be used by get_NN_template()
    """
    import os

    if os.path.exists(filename):
        return open(filename).read()
    else:
        return template
