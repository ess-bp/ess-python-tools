from . import TraceWin
from . import installed
from . import lib_tw
from . import fieldmap
from .nextcloud import nextcloud

try:
    from importlib import metadata
except ImportError:
    # Running on pre-3.8 Python; use importlib-metadata package
    import importlib_metadata as metadata  # type: ignore

__all__ = [
    "fieldmap",
    "installed",
    "lib_tw",
    "relativity",
    "TraceWin",
    "ttf.py",
    "nextcloud",
]

try:
    __version__ = metadata.version("ess-python-tools")  # type: ignore
except ModuleNotFoundError:
    # package is not installed
    pass
