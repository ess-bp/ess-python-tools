import os
import sys
from ess import TraceWin

if os.path.isfile(sys.argv[1]):
    p = TraceWin.project(sys.argv[1])
    for key in p.keys():
        if "beam1_twiss" in key:
            print(key.split("_")[-1], p.get(key))
