"""
Set of functions to calculate the relativistic gamma, beta and energy from each other.
-- M. Eshraqi 2018 July 11
"""


def beta(energy, mass):
    """
    returns relativistic beta by using energy and mass of particle in MeV and
    """
    beta = (1 - gamma(energy, mass) ** -2) ** 0.5
    return beta


def beta_from_gamma(gamma):
    """
    returns relativistic beta by using gamma
    """
    if gamma < 1:
        print("gamma cannot be less than one")
        beta = 0
    else:
        beta = (1 - gamma**-2) ** 0.5
    return beta


def gamma_from_beta(beta):
    """
    returns relativistic gamma by using beta
    """
    if beta == 1:
        import math

        gamma = math.inf
    else:
        gamma = (1 - beta**2) ** -0.5
    return gamma


def gamma(energy, mass):
    """
    returns relativistic gamma by using energy and mass of particle in MeV and
    """
    # if mass>0:
    gamma = 1 + (energy / mass)
    return gamma


def energy(gamma_beta, mass):
    """
    returns energy by using relativistic gamma_beta and mass of particle in MeV.
    if gamma_beta > 1 the value is gamma
    example:
    myrel.energy(2.55, 938)
    > 1453.8999999999999
    if gamma_beta < 1 the value is beta
    example:
    myrel.energy(.55, 938)
    > 185.13182200743233
    """
    if gamma_beta > 1:
        energy = (gamma_beta - 1) * mass
    elif gamma_beta < 1:
        energy = mass * (-1 + 1 / (1 - gamma_beta**2) ** 0.5)
    return energy


def c():
    return 299792458
