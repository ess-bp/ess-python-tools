"""
Now that TraceWin can also convery binary fieldmaps to ASCII the main usage is reduced to trimming fieldmaps
-- M. Eshraqi 2018 July 09



The field map file syntax is the following in the BINARY format:
- Dimension 1 :
    nz (integer 4 bytes) zmax (double 8 bytes) Norm (double 8 bytes)
    for k=0 to nz
        Fz(k.zmax/nz) (float 4 bytes)

- Dimension 2 :
    nz (integer 4 bytes) zmax (double 8 bytes) nx (integer 4 bytes) xmax (double 8 bytes) Norm (double 8 bytes)
    for k=0 to nz
        for i=0 to nr
            Fz(k*zmax/nz, i*rmax/nr) (float 4 bytes)

- Dimension 3 :
(Be careful to the dimention order)
    nz (integer 4 bytes) zmax (double 8 bytes)
    nx (integer 4 bytes) xmin (double 8 bytes) xmax (double 8 bytes)
    ny (integer 4 bytes) ymin (double 8 bytes) ymax (double 8 bytes)
    Norm (double 8 bytes)
    for k=0 to nz
        for j=0 to ny
            for i=0 to nx
                Fz(k*zmax/nz, ymin+j*(ymax-ymin)/ny, xmin+i*(xmax-xmin)/nx) (float 4 bytes)
Warning: The lattice has to be regular.
The normalization factor is equal to ke/Norm or kb/Norm.
Fz are in MV/m for electric field or in T for magnetic field. The dimensions are in meter.
"""

import os
import numpy as np

# import pandas as pd


def read_binary_fieldmap(path_plus_file, fieldmap_dim):
    """
    takes a filename for a binary fieldmap and dimension of the fieldmap,
    1D = 1, 2D = 2, 3D = 3
    and returns a numpy array, with the same format as the TraceWin fieldmap, without the header in ASCII
    """

    File = open(os.path.join(mypath, f), "r")
    # print(f)

    if fieldmap_dim == 3:
        Header_Type = np.dtype(
            [
                ("Nz", "int32"),
                ("Zmax", "float64"),
                ("Nx", "int32"),
                ("Xmin", "float64"),
                ("Xmax", "float64"),
                ("Ny", "int32"),
                ("Ymin", "float64"),
                ("Ymax", "float64"),
                ("Norm", "float64"),
            ]
        )
        THeader = np.fromfile(File, dtype=Header_Type, count=1)
        Nz = THeader["Nz"][0]
        Zmax = THeader["Zmax"][0]
        Nx = THeader["Nx"][0]
        Xmin = THeader["Xmin"][0]
        Xmax = THeader["Xmax"][0]
        Ny = THeader["Ny"][0]
        Ymin = THeader["Ymin"][0]
        Ymax = THeader["Ymax"][0]
        Norm = THeader["Norm"][0]
        Header = np.zeros(len(THeader))
        Header = np.array([Nz, Zmax, Nx, Xmin, Xmax, Ny, Ymin, Ymax, Norm])
    elif fieldmap_dim == 2:
        Header_Type = np.dtype([("Nz", "int32"), ("Zmax", "float64"), ("Nx", "int32"), ("Xmax", "float64"), ("Norm", "float64")])
        Header = np.fromfile(File, dtype=Header_Type, count=1)
        Nz = THeader["Nz"][0]
        Zmax = THeader["Zmax"][0]
        Nx = THeader["Nx"][0]
        Xmax = THeader["Xmax"][0]
        Norm = THeader["Norm"][0]
        Header = np.zeros(len(THeader))
    elif fieldmap_dim == 1:
        Header_Type = np.dtype([("Nz", "int32"), ("Zmax", "float64"), ("Norm", "float32")])
        Header = np.fromfile(File, dtype=Header_Type, count=1)
        Nz = THeader["Nz"][0]
        Zmax = THeader["Zmax"][0]
        Norm = THeader["Norm"][0]
        Header = np.zeros(len(THeader))

    Field_Type = np.dtype([("F_val", "float32")])

    Nf = int(Nz + 1)
    if fieldmap_dim == 2:
        Nf *= int(Nx + 1)
    elif fieldmap_dim == 3:
        Nf *= int(Ny + 1) * int(Nx + 1)
    # print(Nf)

    Field = np.fromfile(File, dtype=Field_Type, count=Nf)

    # print(Field)
    # plot(Field)
    return Header, Field


def cut_fieldmap_length(Field, Header, fieldmap_dim, entrance_rows_cut, exit_rows_cut):
    """
    Takes a Field (ASCII numpy array with TraceWin fieldmap data format),
    dimesion of the fieldmap,
    number of data rows to be removed from array from the entrance side of the field, and
    number of data rows to be removed from array from the exit side of the field
    and returns a numpy array which contains only the remaining rows.
    """
    # print(Header[0])
    lowx = np.arange(0, int(entrance_rows_cut))
    # print('lowx', lowx)
    highx = np.arange(int(Header[0] + 1 - exit_rows_cut), int(Header[0] + 1))
    # print('highx', highx)
    newNz = (Header[0] + 1) - entrance_rows_cut - exit_rows_cut
    stepZ = Header[1] / Header[0]

    if fieldmap_dim == 1:
        newlength = newNz
    elif fieldmap_dim == 2:
        Fieldmapmatrix = np.reshape(Field, (int(Header[0] + 1), int(Header[2] + 1)))
        newlength = newNz * (Header[2] + 1)
    elif fieldmap_dim == 3:
        Fieldmapmatrix = np.reshape(Field, (int(Header[0] + 1), int((Header[2] + 1) * (Header[5] + 1))))
        # print(Fieldmapmatrix.shape)
        newlength = newNz * (Header[2] + 1) * (Header[5] + 1)

    if entrance_rows_cut > 0 and exit_rows_cut > 0:
        x = np.delete(Fieldmapmatrix, (lowx, highx), axis=0)
    elif entrance_rows_cut == 0:
        x = np.delete(Fieldmapmatrix, (highx), axis=0)
    elif exit_rows_cut == 0:
        x = np.delete(Fieldmapmatrix, (lowx), axis=0)

    ShortenedMatrix = np.reshape(x, (int(newlength), 1))

    Header[0] = newNz - 1
    Header[1] = stepZ * (newNz - 1)

    return Header, ShortenedMatrix


"""
The parameters in the following six rows define the path to your fieldmap, no name is needed, this script
scans the folder and converts all files, assumes all are binary fieldmaps, to ASCII fieldmaps.
The dimension of the field is also needed.
If you need to cut the field, the n_enter and n_exit should be adjusted.

#  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #
#                                                                              #
#            C h a n g e   t h e   f o l l o w i n g    v a l u e s            #
#                                                                              #
#  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #
"""

mypath = "Data/FM/"  # where the fieldmaps are stored
fieldmap_dim = 3
# fieldmap_dim = 2
# fieldmap_dim = 1
n_enter = 0  # Number of lines to be removed from entrance of fieldmap
n_exit = 0  # Number of lines to be removed from exit of fieldmap

onlyfiles = [f for f in os.listdir(mypath) if os.path.isfile(os.path.join(mypath, f))]

for f in onlyfiles:
    if f[0] != ".":
        Header, Field = read_binary_fieldmap(f, fieldmap_dim)

        if n_enter + n_exit > 0:
            Header, Field = cut_fieldmap_length(Field, Header, fieldmap_dim, n_enter, n_exit)

        with open(os.path.join(mypath, "ASCII", f), "w") as out_file:
            stepZ = Header[1] / Header[0]  # stepZ = Zmax / Nz

            out_file.write(" ".join(str(hval) for hval in Header[0:2]))
            out_file.write("\n")
            if fieldmap_dim == 1:
                # Norm = Header[2]

                # out_file.write(' '.join(str(hval) for hval in Header[0:2]))
                # out_file.write('\n')
                out_file.write(" ".join(str(Header[2])))
                out_file.write("\n")

            elif fieldmap_dim == 2:
                # Norm = Header[4]
                stepX = Header[3] / Header[2]  # stepX = Xmax / Nx

                # out_file.write(' '.join(str(hval) for hval in Header[0:2]))
                # out_file.write('\n')
                out_file.write(" ".join(str(hval) for hval in Header[2:4]))
                out_file.write("\n")
                out_file.write(" ".join(str(Header[4])))
                out_file.write("\n")

            elif fieldmap_dim == 3:
                # Norm = Header[8]
                stepX = (Header[4] - Header[3]) / Header[2]  # stepX = (Xmax - Xmin) / Nx
                stepY = (Header[7] - Header[6]) / Header[5]  # stepY = (Ymax - Ymin) / Ny

                # out_file.write((' '.join(str(hval) for hval in Header[0:2])))
                # out_file.write('\n')
                out_file.write((" ".join(str(hval) for hval in Header[2:5])))
                out_file.write("\n")
                out_file.write((" ".join(str(hval) for hval in Header[5:8])))
                out_file.write("\n")
                out_file.write("".join(str(Header[8])))
                out_file.write("\n")

            for fval in Field:
                out_file.write(str(fval[0][0]))
                out_file.write("\n")  # print(fval)
