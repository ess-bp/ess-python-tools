from ess import nextcloud
import h5py

nc = nextcloud()

# Get a list of files and folders in a path
file_list = nc.get_filelist("LCR/Saved PVs")

# Get a list of files and folders matching a pattern
emu_folders = nc.get_filelist("LCR/Saved PVs/2019-0*/EMU")

# Read a specific file
file_path = nc.get_resource(
    "LCR/Saved PVs/2019-04-26/EMU/400W_130-68.5-219A_3.70sccm/p-tank_S1-311A_S2-260A/INFN-SRC:INTK-EMU-1-Slit:TEMP_2019-04-26_17-50-24.hdf5"
)
h5py.File(file_path, "r")
