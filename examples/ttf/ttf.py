from ess import ttf, fieldmap
import numpy as np

Header, Field = fieldmap.read_fieldmap("Data/FM/HB_W_coupler.edz", 3, "b")
fz = ttf.field_on_axis(Field, 3, Header[0], Header[2], Header[5])
z = np.arange(0, int(Header[0] + 1))
dz = Header[1] / Header[0]
z_points = dz * z

print(
    "Int_E:",
    ttf.Field_integral(fz, dz),
    "[TTF, Phase_Max_Energy]: ",
    ttf.Field_TTF(fz, dz, 704.42, 700, 938, True),
)
TTFb = ttf.TTF_beta(fz, dz, 704.42, 938)

# plot(z_points, fz);
# plot(TTFb[0,:], 50*TTFb[1,:]);
