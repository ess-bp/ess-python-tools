def define_and_handle_args():
    import argparse

    parser = argparse.ArgumentParser(description="Simple setup script to run TraceWin on HT Condor..")

    parser.add_argument("-s", dest="seed", default=None, type=int, help="Define seed")

    parser.add_argument("-c", dest="calc_dir", default=".", type=str, help="Path to calculation folder")

    parser.add_argument("-p", dest="priority", default=0, type=int, help="Job priority")

    return parser.parse_args()


def setup_exec_folder():
    import os
    import shutil

    orig_path = os.path.join(os.environ.get("HOME"), "TraceWin_exe")
    if not os.path.isdir(orig_path):
        raise ValueError("Cannot find tracewin executables in " + orig_path + " yet")
    os.makedirs("TraceWin_exe")

    o = dict(LINUX="lx", OSX="mac")
    a = dict(INTEL="", X86_64="64")

    for OpSys in ["LINUX", "OSX"]:
        for Arch in ["INTEL", "X86_64"]:
            shutil.copy(
                os.path.join(orig_path, "trace" + o[OpSys] + a[Arch]),
                os.path.join("TraceWin_exe", "tracewin.%s.%s" % (OpSys, Arch)),
            )


def setup(args):
    import os
    import random

    num_jobs = 0
    for folder in os.listdir(args.calc_dir):
        if folder[0:15] == "Local_TraceWin_":
            num_jobs += 1

    if num_jobs == 0:
        raise ValueError("Could not find any simulation folders in " + args.calc_dir)

    if not os.path.isdir("TraceWin_exe"):
        setup_exec_folder()

    if args.seed is not None:
        random.seed(args.seed)
    rand_lst = [random.randint(1e5, 1e9) for i in range(num_jobs)]
    # rand_lst=file('rand.dat','r').read().split()

    # Get file list:
    input_files = os.listdir(os.path.join(args.calc_dir, "Local_TraceWin_0"))
    for i in range(len(input_files)):
        if input_files[i][-2:] == "_0":
            input_files[i] = input_files[i][:-2] + "_$(Process)"
    input_files = ", ".join(input_files)

    head_template = open("head.job.template", "r").read()
    queue_template = open("queue.job.template", "r").read()
    with open("submit.job", "w") as out:
        out.write(head_template.format(**locals()))

        for i in range(num_jobs):
            rand = rand_lst[i]
            out.write(queue_template.format(**locals()))


def execute():
    import subprocess

    cmd = ["condor_submit", "submit.job"]
    subprocess.call(cmd)


if __name__ == "__main__":
    args = define_and_handle_args()
    setup(args)
    execute()
