## ESS Beam Physics Python Tools

This project provides useful Python tools for executing TraceWin, and reading/writing TraceWin data files.

The files are written primarily for usage at European Spallation Source, but might be useful elsewhere.

You are free to use this code, but no warranty whatsoever is given.

### Installation

If you want to install this on the ESS Jupyter server or similar ESS configured computer, then run

```bash
pip install ess-python-tools -U
```

If you wish to use our artifactory server then you can add https://artifactory.esss.lu.se/artifactory/api/pypi/pypi-virtual/simple as your index-url or as extra-index-url in your [pip.conf](https://pip.pypa.io/en/stable/user_guide/#id19).

To install/ upgrade the package, run the command below. It is assumed that you have git and pip installed on your system.

```bash
pip install git+https://gitlab.esss.lu.se/ess-bp/ess-python-tools.git -U
```

The argument `-U` implies you should upgrade if you have already installed this package but a newer exist.


### Developers

Development is done directly to the master branch for now, without much in terms of official approval. Try to use your powers wisely (and no worries, sometimes it breaks and we fix it).

Tags are automatically pushed to our internal artifactory server, so users can install pre-compiled binaries using pip.

It is recommended that you set up a couple of git hooks to avoid test failures. Set `pre-commit run` as the pre-commit hook, and `python tests.py` as your pre-push hook. If you do not know how to set up hooks, ask Yngve.

### Usage

Please see the [Online Documentation](http://ess-bp.pages.esss.lu.se/ess-python-tools).
